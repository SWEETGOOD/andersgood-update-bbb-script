#!/bin/bash
set -o nounset   # set -u : exit the script if you try to use an uninitialised variable
set -o errexit   # set -e : exit the script if any statement returns a non-true return value

#                                                                        #
#   _______          ________ ______ _______ _____  ____   ____  _____   #
#  / ____\ \        / /  ____|  ____|__   __/ ____|/ __ \ / __ \|  __ \  #
# | (___  \ \  /\  / /| |__  | |__     | | | |   _| |  | | |  | | |  | | #
#  \___ \  \ \/  \/ / |  __| |  __|    | | | |  | | |  | | |  | | |  | | #
#  ____) |  \  /\  /  | |____| |____   | | | |__| | |__| | |__| | |__| | #
# |_____/    \/  \/   |______|______|  |_|  \_____|\____/ \____/|_____/  #
#                                                                        #
#                              IT-Beratung mit Fokus auf Datensicherheit #
#                                                                        #
#                            www.sweetgood.de                            #
#                                                                        #
# Copyright        : All rights reserved!
# Repository url   : codeberg.org/SWEETGOOD/andersgood-update-bbb-script
# Author           : codiflow @ SWEETGOOD
# Filename         : update-bbb.sh
# Created at       : 17.08.2020
# Last changed at  : 25.11.2023
# Description      : Updates BigBlueButton and backs up configurations and
#                    files beforehand
# Compatibility    : v1.3 => BigBlueButton 2.2.x - 2.4.x
#                    v1.4 => BigBlueButton 2.5.x (untested) - 2.6.x
#                    v1.5 => BigBlueButton 2.6.x - 2.7.x

# ---
# PLEASE CHANGE THE VALUES BELOW THIS LINE
# ---

BBBIP="my.server.de"
LETSENCRYPTEMAIL="mail@server.de"
BACKUPROOTFOLDER="/root/bigbluebutton-sg/backups"

# The following string is empty by default, so this script will update BigBlueButton using "apt upgrade".
# You can enter your bbb-install.sh string here which will be executed instead if entered.
# You find the details on this string here: https://github.com/bigbluebutton/bbb-install#command-options
# Example: wget -qO- https://raw.githubusercontent.com/bigbluebutton/bbb-install/v2.7.x-release/bbb-install.sh | bash -s -- -v focal-270 -s ${BBBIP} -e ${LETSENCRYPTEMAIL} -g -w

BBBINSTALLSCRIPTSTRING=""

# ---
# DON'T CHANGE ANYTHING BELOW THIS LINE UNLESS YOU KNOW WHAT YOU ARE DOING
# ---

VERSION='1.5'
RED='\e[31m'
BLUE='\e[36m'
NC='\e[39m' # No Color
BACKUPFOLDER="$(date +%Y%m%d_%H%M%S)_$(hostname)_${VERSION}"
BACKUPPATH="${BACKUPROOTFOLDER}/${BACKUPFOLDER}"

# root check
if [ "$(id -u)" -ne 0 ]
  then echo "Please start the script as user ${RED}root${NC}."
  exit
fi

# Create backup root folder if it doesn't exist
if [ ! -d "${BACKUPROOTFOLDER}" ]; then
  mkdir -p "${BACKUPROOTFOLDER}"
fi

echo -e "${BLUE}#####################################${NC}"
echo -e "${BLUE}# BigBlueButton upgrade script v${VERSION} #${NC}"
echo -e "${BLUE}#          www.sweetgood.de         #${NC}"
echo -e "${BLUE}#####################################${NC}"
echo " "
echo -e "${BLUE}This script is configured for server: ${RED}${BBBIP}${NC}"
echo " "
echo "---"
echo " "
read -n 1 -r -s -p $'Do you want to start the update of BigBlueButton now?\n'
echo " "
echo "---"
echo " "
echo -e "${BLUE}Starting the configuration backup${NC}"
echo " "
cd "${BACKUPROOTFOLDER}"
echo "> Creating backup folder"
mkdir "${BACKUPFOLDER}"
echo "> Creating backup of the STUN/TURN server configuration"
cp /usr/share/bbb-web/WEB-INF/classes/spring/turn-stun-servers.xml "${BACKUPPATH}"
cp /etc/bigbluebutton/turn-stun-servers.xml "${BACKUPPATH}"/turn-stun-servers.xml-override || true
echo "> Creating backup of the BBB configuration"
cp /usr/share/bbb-web/WEB-INF/classes/bigbluebutton.properties "${BACKUPPATH}"
cp /etc/bigbluebutton/bbb-web.properties "${BACKUPPATH}"/bbb-web.properties-override || true
echo "> Creating backup of the default presentation"
cp /var/www/bigbluebutton-default/assets/default* "${BACKUPPATH}"
echo "> Creating backup of meteor settings"
cp /usr/share/meteor/bundle/programs/server/assets/app/config/settings.yml "${BACKUPPATH}"
cp /etc/bigbluebutton/bbb-html5.yml "${BACKUPPATH}"/bbb-html5.yml-override || true
cp /usr/share/meteor/bundle/bbb-html5-with-roles.conf "${BACKUPPATH}"
cp /etc/bigbluebutton/bbb-html5-with-roles.conf "${BACKUPPATH}"/bbb-html5-with-roles.conf-override || true
echo "> Creating backup of freeswitch settings"
cp /opt/freeswitch/etc/freeswitch/autoload_configs/conference.conf.xml "${BACKUPPATH}"
cp /opt/freeswitch/conf/sip_profiles/external.xml "${BACKUPPATH}"
cp /opt/freeswitch/conf/vars.xml "${BACKUPPATH}"
echo "> Creating backup of VoIP implementation of freeswitch (if any)"
mkdir "${BACKUPPATH}"/sip_profiles
mkdir "${BACKUPPATH}"/dialplan
cp /opt/freeswitch/conf/sip_profiles/external/*.xml "${BACKUPPATH}"/sip_profiles
cp /opt/freeswitch/conf/dialplan/public/*.xml "${BACKUPPATH}"/dialplan
echo "> Creating backup of kurento settings"
cp /usr/local/bigbluebutton/bbb-webrtc-sfu/config/default.yml "${BACKUPPATH}"
cp /etc/bigbluebutton/bbb-webrtc-sfu/production.yml "${BACKUPPATH}"/production.yml-override || true
cp /usr/local/bigbluebutton/bbb-pads/config/settings.json "${BACKUPPATH}"
cp /etc/bigbluebutton/bbb-pads.json "${BACKUPPATH}"/bbb-pads.json-override || true
echo "> Creating backup of recording settings"
cp /usr/local/bigbluebutton/core/scripts/bigbluebutton.yml "${BACKUPPATH}"
cp /etc/bigbluebutton/recording/recording.yml "${BACKUPPATH}"/recording.yml-override || true
cp /usr/local/bigbluebutton/core/scripts/presentation.yml "${BACKUPPATH}"
cp /etc/bigbluebutton/recording/presentation.yml "${BACKUPPATH}"/presentation.yml-override || true
echo "> Creating backup of cron settings"
cp /etc/cron.daily/bigbluebutton "${BACKUPPATH}"
cp /etc/default/bigbluebutton-cron-config "${BACKUPPATH}"/bigbluebutton-cron-config-override || true
echo "> Creating backup of nginx settings"
cp /usr/share/bigbluebutton/nginx/sip.nginx "${BACKUPPATH}"
echo "> Creating backup of apply-config.sh"
cp /etc/bigbluebutton/bbb-conf/apply-config.sh "${BACKUPPATH}"
echo "> Get current settings and write them into a backup file"
bbb-conf --check > "${BACKUPPATH}"/config-pre-update.log
echo " "
echo -e "${BLUE}Backup of configurations done.${NC}"
echo " "
echo "---"
echo " "
read -n 1 -r -s -p $'Press any key to proceed with the update of BigBlueButton...\n'
echo " "

if [ "${BBBINSTALLSCRIPTSTRING}" == "" ]; then
  echo -e "${BLUE}Update process ${RED}using apt${BLUE} started.${NC}"
  echo " "
  echo "> Renew apt-cache"
  echo " "
  apt update
  echo " "
  echo -e "> ${RED}The following packages are getting updated. Please confirm manually or abort the process by pressing [STRG] + [C]...${NC}"
  echo " "
  apt upgrade
  echo " "
  echo -e "${BLUE}Update process ${RED}using apt${BLUE} finished.${NC}"
else
  echo -e "${BLUE}Update process ${RED}using bbb-install.sh${BLUE} started.${NC}"
  echo " "
  echo "> Running bbb-install.sh string"
  echo " "
  /bin/bash -c "${BBBINSTALLSCRIPTSTRING}"
  echo " "
  echo -e "${BLUE}Update process ${RED}using bbb-install.sh${BLUE} finished.${NC}"
fi

echo " "
echo "---"
echo " "
read -n 1 -r -s -p $'Press any key to copy some files back from backup to production...\n'
echo " "
echo -e "${BLUE}Copying of configuration backups started.${NC}"
echo " "
echo "> Copy default presentation as pdf file from backup to production"
mv /var/www/bigbluebutton-default/assets/default.pdf /var/www/bigbluebutton-default/assets/default.pdf.dist
cp "${BACKUPPATH}"/default.pdf /var/www/bigbluebutton-default/assets/default.pdf

if [ "${BBBINSTALLSCRIPTSTRING}" == "" ]; then
  echo "> Running setip command with bbb-conf"
  echo " "
  bbb-conf --setip ${BBBIP}
  echo " "
fi

echo -e "${BLUE}Copying of configuration backups finished.${NC}"
echo " "
echo "---"
echo " "
echo -e "${BLUE}Compare new configuration to the configuration from the backup${NC}"
echo " "
bbb-conf --check > "${BACKUPPATH}"/config-post-update.log
echo " "
diff "${BACKUPPATH}"/config-pre-update.log "${BACKUPPATH}"/config-post-update.log
echo " "
echo -e "${BLUE}Script finished.${NC} You'll find the backups and logs at ${BLUE}""${BACKUPPATH}""${NC}"
echo " "

