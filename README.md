# update-bbb.sh

Updates BigBlueButton and backs up relevant configuration files beforehand.

[Related blog article on BBB server updates (till 2.4 and German only)](https://andersgood.de/blog/bigbluebutton-grundlegend-anpassen#updates-installieren)

[All my German blog articles about BigBlueButton](https://andersgood.de/suche?tags=BigBlueButton)

----

## Usage

1. Check which version of the script you need:

v1.3: BBB 2.2 - BBB 2.4  
v1.4: BBB 2.5 (not tested) - BBB 2.6  
v1.5: BBB 2.7

2. **Download the file** `update-bbb.sh` using

v1.3 (BBB 2.2 - BBB 2.4): `wget https://codeberg.org/SWEETGOOD/andersgood-update-bbb-script/raw/tag/v1.3/update-bbb.sh`  
v1.4 (BBB 2.5 - BBB 2.6): `wget https://codeberg.org/SWEETGOOD/andersgood-update-bbb-script/raw/tag/v1.4/update-bbb.sh`  
v1.5 (BBB 2.7): `wget https://codeberg.org/SWEETGOOD/andersgood-update-bbb-script/raw/tag/v1.5/update-bbb.sh`

3. **Edit the file** using `vi`, `nano` or your favorite text editor.

4. **Change the following variables** at the beginning of the script:

| Variable | Description |
| --- | --- |
| `BBBIP` | The domain name your BBB will be reachable from the internet |
| `LETSENCRYPTEMAIL` | If you issued the Let's Encrypt SSL cert while BBB installation put your email address here |
| `BACKUPROOTFOLDER` | The folder where you want the config file backups to be placed (will automatically be created if it doesn't exist) |
| `BBBINSTALLSCRIPTSTRING` | If empty the installer uses `apt get` for updating. If you put the full command line you used for the installation of your server here, it will be used instead.<br /><br />**I STRONGLY RECOMMEND TO USE THIS METHOD AND FILL IN YOUR INSTALL STRING HERE BECAUSE THE APT GET METHOD DOES NOT UPDATE GREENLIGHT!** |

5. **Save the file**.

6. **Make it executable** with `chmod +x ./update-bbb.sh`

7. **Execute it** with `./update-bbb.sh`

----

## Changelog

**v1.5** - 25.11.2023  
Script is now [shellcheck](https://www.shellcheck.net/) approved 🥳  
Updated bbb-install.sh repository to BBB 2.7  
Added version number of script to backup folder name  

**v1.4** - 02.04.2023  
Now also backs up the override files and **all** files with relevant settings  
Updated bbb-install.sh repository to BBB 2.6  

**v1.3** - 05.03.2022  
Added bash error handling
Updated bbb-install.sh repository to BBB 2.4  

**v1.2** - 02.06.2021  
Added bbb-install.sh as alternative update method  
Translated script to english  
Backup now contains more relevant configurations  
Restructured script with comments  

**v1.1** - 20.01.2021  
Fixed hardcoded backup root folder path  

**v1.0** - 17.08.2020  
Initial release
